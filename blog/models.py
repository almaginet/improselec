# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify
from django.core.urlresolvers import reverse
from django.utils.timezone import now

import os
import datetime

class Config(models.Model):
    webName = models.CharField(max_length=300, verbose_name="Titulo")
    webSlogan = models.CharField(max_length=300, verbose_name="Slogan action")
    webUrl = models.CharField(max_length=300, verbose_name="Url")
    addons = models.TextField(blank=True, verbose_name="addons")
    favicon = models.FileField(upload_to='favicon', blank=True, null=True)
    description = models.TextField(blank=True, verbose_name="Contenido")
    color = models.TextField(blank=True, verbose_name="Color Principal menu izquierdo")
    color02 = models.TextField(blank=True, verbose_name="Color menu derecho")
    color03 = models.TextField(blank=True, verbose_name="Color menu top")
    color04 = models.TextField(blank=True, verbose_name="Redes")
    logo = models.ImageField(upload_to='config', blank=True, null=True)
    videoMp4 = models.FileField(upload_to='videos/', blank=True, null=True)
    videoOgv = models.FileField(upload_to='videos/', blank=True, null=True)
    videoWebm = models.FileField(upload_to='videos/', blank=True, null=True)
    background = models.ImageField(upload_to='config', blank=True, null=True)
    create_at = models.DateTimeField(default=now, editable=False)
    update_at = models.DateTimeField(auto_now_add = False, auto_now=True, editable=False)
    slug = models.SlugField(editable=False)

    class Meta:
        ordering = ['create_at']
        verbose_name = "Configuracion"
        verbose_name_plural = 'Configuraciones'

    def __unicode__(self):
        return self.webName

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.webName)
        super(Config, self).save(*args, **kwargs)

class Fabricante(models.Model):

    titulo = models.CharField(verbose_name='titulo', max_length=100, unique=True)
    descripcion = models.TextField()
    foto = models.ImageField(upload_to='articles/backgrounds', blank=True, null=True, verbose_name="Foto")
    url = models.CharField(blank=True, max_length=200, verbose_name="Web")
    create_at = models.DateTimeField(default=now, editable=False)
    update_at = models.DateTimeField(auto_now_add = False, auto_now=True, editable=False)
    slug = models.SlugField(editable=False,max_length=144)

    class Meta:
            ordering = ['create_at']
            verbose_name = 'Fabricante'
            verbose_name_plural = 'Fabricantes'

    def __unicode__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.titulo)
        super(Fabricante, self).save(*args, **kwargs)

class Categoria(models.Model):

    titulo = models.CharField(verbose_name='titulo', max_length=100, unique=True)
    descripcion = models.TextField()
    foto = models.ImageField(upload_to='articles/backgrounds', blank=True, null=True, verbose_name="Foto")
    padre = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)
    create_at = models.DateTimeField(default=now, editable=False)
    update_at = models.DateTimeField(auto_now_add = False, auto_now=True, editable=False)
    slug = models.SlugField(editable=False,max_length=144)

    class Meta:
            ordering = ['create_at']
            verbose_name = 'Categoria'
            verbose_name_plural = 'Categorias'

    def __unicode__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.titulo)
        super(Categoria, self).save(*args, **kwargs)

class Producto(models.Model):

    titulo = models.CharField(verbose_name='titulo', max_length=100, unique=True)
    descripcion = models.TextField()
    foto = models.ImageField(upload_to='articles/backgrounds', blank=True, null=True, verbose_name="Foto")
    precio = models.CharField(blank=True, max_length=20, verbose_name="Precio")
    oferta = models.CharField(blank=True, max_length=20, verbose_name="Precio Oferta")
    codigoUniversal = models.CharField(blank=True, max_length=20, verbose_name="Codigo Universal")
    codigoLocal = models.CharField(blank=True, max_length=20, verbose_name="Codigo Local")
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, blank=True, null=True, related_name='categorias')
    fabricante = models.ForeignKey(Fabricante, on_delete=models.CASCADE, blank=True, null=True, related_name='fabricante')
    create_at = models.DateTimeField(default=now, editable=False)
    update_at = models.DateTimeField(auto_now_add = False, auto_now=True, editable=False)
    slug = models.SlugField(editable=False,max_length=144)
    destacado = models.BooleanField(blank=True, verbose_name="¿en destacados?")
    oferta = models.BooleanField(blank=True, verbose_name="¿en oferta?")

    class Meta:
            ordering = ['create_at']
            verbose_name = 'Producto'
            verbose_name_plural = 'Productos'

    def __unicode__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.titulo)
        super(Producto, self).save(*args, **kwargs)


class Articulo(models.Model):

    titulo = models.CharField(verbose_name='titulo', max_length=100, unique=True)
    contenido = models.TextField()
    foto = models.ImageField(upload_to='articles/backgrounds', blank=True, null=True, verbose_name="Foto")
    create_at = models.DateTimeField(default=now, editable=False)
    update_at = models.DateTimeField(auto_now_add = False, auto_now=True, editable=False)
    slug = models.SlugField(editable=False,max_length=144)

    class Meta:
            ordering = ['create_at']
            verbose_name = 'Articulo'
            verbose_name_plural = 'Articulos'

    def __unicode__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.titulo)
        super(Articulo, self).save(*args, **kwargs)

class Red(models.Model):

    titulo = models.CharField(verbose_name='titulo', max_length=100, unique=True)
    link = models.TextField()
    foto = models.ImageField(upload_to='redes', blank=True, null=True, verbose_name="Foto")
    create_at = models.DateTimeField(default=now, editable=False)
    update_at = models.DateTimeField(auto_now_add = False, auto_now=True, editable=False)
    slug = models.SlugField(editable=False,max_length=144)

    class Meta:
            ordering = ['create_at']
            verbose_name = 'Red'
            verbose_name_plural = 'Redes'

    def __unicode__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.titulo)
        super(Red, self).save(*args, **kwargs)