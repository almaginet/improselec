from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.views import generic
from django.conf import settings
from django.core.urlresolvers import reverse_lazy

from .models import *
from accounts.models import *


def ArticleListView(request):

    template = 'blog/home.html'
    producto = Producto.objects.all().order_by('-create_at')
    config = Config.objects.all()
    categoria = Categoria.objects.all().order_by('-create_at')
    red = Red.objects.all().order_by('-create_at')
    fabricante = Fabricante.objects.all().order_by('-create_at')
    home = True
    return render_to_response(template,locals())

def CategoriaDetailView(request, slug):

    template = 'blog/categoria_detail.html'
    link = Categoria.objects.get(slug = slug)
    categoria = Categoria.objects.all().order_by('-create_at')
    categoria01 = Categoria.objects.all().order_by('-create_at')
    categoria02 = Categoria.objects.all().order_by('-create_at')
    categoria03 = Categoria.objects.all().order_by('-create_at')
    fabricante = Fabricante.objects.all().order_by('-create_at')
    producto = Producto.objects.all().order_by('-create_at')
    red = Red.objects.all().order_by('-create_at')
    config = Config.objects.all()
    a = 1
    return render_to_response(template,locals())

def ProductoDetailView(request, slug):

    template = 'blog/producto_detail.html'
    link = Producto.objects.get(slug = slug)
    producto = Producto.objects.all().order_by('-create_at')
    config = Config.objects.all()
    categoria = Categoria.objects.all().order_by('-create_at')
    categoria01 = Categoria.objects.all().order_by('-create_at')
    categoria02 = Categoria.objects.all().order_by('-create_at')
    categoria03 = Categoria.objects.all().order_by('-create_at')
    fabricante = Fabricante.objects.all().order_by('-create_at')
    red = Red.objects.all().order_by('-create_at')
    return render_to_response(template,locals())

def Somos(request):

    template = 'blog/somos.html'
    producto = Producto.objects.all().order_by('-create_at')
    config = Config.objects.all()
    categoria = Categoria.objects.all().order_by('-create_at')
    categoria01 = Categoria.objects.all().order_by('-create_at')
    categoria02 = Categoria.objects.all().order_by('-create_at')
    categoria03 = Categoria.objects.all().order_by('-create_at')
    red = Red.objects.all().order_by('-create_at')
    return render_to_response(template,locals())

def Contacto(request):

    template = 'blog/contacto.html'
    producto = Producto.objects.all().order_by('-create_at')
    config = Config.objects.all()
    categoria = Categoria.objects.all().order_by('-create_at')
    categoria01 = Categoria.objects.all().order_by('-create_at')
    categoria02 = Categoria.objects.all().order_by('-create_at')
    categoria03 = Categoria.objects.all().order_by('-create_at')
    red = Red.objects.all().order_by('-create_at')
    return render_to_response(template,locals())

def FabricanteDetailView(request, slug):

    template = 'blog/fabricante_detail.html'
    link = Fabricante.objects.get(slug = slug)
    fabricante = Fabricante.objects.all().order_by('-create_at')
    producto = Producto.objects.all().order_by('-create_at')
    config = Config.objects.all()
    categoria = Categoria.objects.all().order_by('-create_at')
    categoria01 = Categoria.objects.all().order_by('-create_at')
    categoria02 = Categoria.objects.all().order_by('-create_at')
    categoria03 = Categoria.objects.all().order_by('-create_at')
    red = Red.objects.all().order_by('-create_at')
    q = Producto.objects.values('categoria').distinct()
    return render_to_response(template,locals())

def ArticuloDetailView(request, slug):

    template = 'blog/articulo_detail.html'
    link = Articulo.objects.get(slug = slug)
    articulo = Articulo.objects.all().order_by('-create_at')
    config = Config.objects.all()
    categoria = Categoria.objects.all().order_by('-create_at')
    categoria01 = Categoria.objects.all().order_by('-create_at')
    categoria02 = Categoria.objects.all().order_by('-create_at')
    categoria03 = Categoria.objects.all().order_by('-create_at')
    red = Red.objects.all().order_by('-create_at')
    return render_to_response(template,locals())

class ArticuloListView(generic.ListView):

    template_model = 'blog/articulo_list.html'
    model = Articulo
    context_object_name = 'articulo'
    paginate_by = 120
    ordering = '-create_at'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(ArticuloListView, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['categoria'] = Categoria.objects.all()
        context['categoria01'] = Categoria.objects.all()
        context['categoria02'] = Categoria.objects.all()
        context['categoria03'] = Categoria.objects.all()
        context['fabricante'] = Fabricante.objects.all().order_by('?')
        context['config'] = Config.objects.all()
        red = Red.objects.all().order_by('-create_at')
        return context

class ProListView(generic.ListView):

    template_model = 'blog/producto_list.html'
    model = Producto
    context_object_name = 'producto'
    paginate_by = 2
    ordering = '-create_at'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(ProListView, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['categoria'] = Categoria.objects.all()
        context['categoria01'] = Categoria.objects.all()
        context['categoria02'] = Categoria.objects.all()
        context['categoria03'] = Categoria.objects.all()
        context['fabricante'] = Fabricante.objects.all().order_by('?')
        context['config'] = Config.objects.all()
        context['red'] = Red.objects.all().order_by('-create_at')
        context['a'] = 1
        return context

#class ProfileListView(generic.ListView):
#
 #   template_model = 'blog/userprofile_list.html'
  #  model = Fabricante
   # context_object_name = 'fabricante'
    #paginate_by = 10
    #ordering = '-create_at'

    #def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
     #   context = super(ProfileListView, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
      #  context['fabricante'] = Fabricante.objects.all()
       # context['config'] = Config.objects.all()
        #return context