from django.conf.urls import url
# -*- encoding: utf-8 -*-
from . import views

urlpatterns = [
    # /blog/ | /
    url(r'^$', 'blog.views.ArticleListView', name='blog.article_list'),
#    url(r'profiles/(?P<slug>[-\w]+)/$', 'blog.views.UserProfileDetailView', name='blog.userprofile_detail'),
	url(r'^somos/', 'blog.views.Somos', name='blog.somos'),
	url(r'^contacto/', 'blog.views.Contacto', name='blog.contacto'),
    url(r'^productos/(?P<slug>[-\w]+)/$', 'blog.views.ProductoDetailView', name='blog.producto_detail'),
    url(r'^blog/(?P<slug>[-\w]+)/$', 'blog.views.ArticuloDetailView', name='blog.articulo_detail'),
    url(r'^blog/$', views.ArticuloListView.as_view(), name='blog.articulo_list'),
    url(r'^productos/$', views.ProListView.as_view(), name='blog.pro_list'),
#    url(r'^perfil/(?P<slug>[-\w]+)/$', 'blog.views.ProfileDetailView', name='blog.profile_detail'),
    url(r'^categorias/(?P<slug>[-\w]+)/$', 'blog.views.CategoriaDetailView', name='blog.categoria_detail'),
    url(r'^fabricantes/(?P<slug>[-\w]+)/$', 'blog.views.FabricanteDetailView', name='blog.fabricante_detail'),
 #   url(r'^blog/(?P<slug>[-\w]+)/$', 'blog.views.BlogDetailView', name='blog.blog'),
#    url(r'^secciones/(?P<slug>[-\w]+)/$', 'blog.views.SectionDetailView', name='blog.section_detail'),
#    url(r'^tags/(?P<slug>[-\w]+)/$', 'blog.views.TagDetailView', name='blog.tag_detail'),
#    url(r'^trend/(?P<slug>[-\w]+)/$', 'blog.views.TrendDetailView', name='blog.trend_detail'),
#    url(r'^crear/$', views.ArticleCreateView.as_view(), name='blog.crear'),
#    url(r'^editar/(?P<slug>[-\w]+)/$', views.ArticleUpdateView.as_view(), name='blog.editar'),
#    url(r'^eliminar/(?P<slug>[-\w]+)/$', views.ArticleDeleteView.as_view(), name='blog.eliminar'),
]