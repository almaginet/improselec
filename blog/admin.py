from django.contrib import admin

from .models import *

class ProductoAdmin(admin.ModelAdmin):

    list_display = ('titulo', 'codigoUniversal', 'codigoLocal', 'categoria', 'fabricante', 'create_at', 'update_at', 'slug')

admin.site.register(Producto, ProductoAdmin)
admin.site.register(Categoria)
admin.site.register(Fabricante)
admin.site.register(Articulo)
admin.site.register(Red)
admin.site.register(Config)